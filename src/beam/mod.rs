// stingray, interfaces for Erlang and the BEAM
// Copyright (c) 2023 rini
//
// SPDX-License-Identifier: Apache-2.0

use crate::{Error, etf::Term};

pub mod code;
pub mod opcode;

#[cfg(feature = "encode")]
mod encode;
#[cfg(feature = "decode")]
mod parse;

/// Current beam format. This number is bumped if the bytecode format ever changes.
pub const BEAM_FORMAT: u32 = 0;

/// Largest known opcode. If a file contains a number bigger than this, we can't parse it.
pub const MAX_OPCODE: u32 = 183;

pub type Atom = Box<str>;

#[derive(Debug)]
#[non_exhaustive]
pub struct Beam {
    /// Interned atom (symbol) table.
    pub atoms: Box<[Atom]>,
    /// The module's code.
    pub code: Box<[code::Instr]>,
    /// Interned string table. Each string is tacked onto eachother, and string literals should be
    /// slices of this string.
    pub strings: Box<str>,
    /// Module imports, including arity and source module.
    pub imports: Box<[Import]>,
    /// Module exports, including arity.
    pub exports: Box<[Export]>,
    pub literals: Box<[Term]>,
}

impl Beam {
    pub fn builder() -> Builder {
        Builder::default()
    }

    #[cfg(feature = "decode")]
    pub fn decode(data: &[u8]) -> Result<Self, Error> {
        parse::Parser::new(data).parse()
    }

    #[cfg(feature = "encode")]
    pub fn encode(&self) -> Vec<u8> {
        encode::Encoder::new(self).encode()
    }
}

#[derive(Debug)]
pub struct Import {
    pub module: u32,
    pub function: u32,
    pub arity: u32,
}

#[derive(Debug)]
pub struct Export {
    pub function: u32,
    pub arity: u32,
    pub label: u32,
}

#[derive(Default)]
pub struct StringTable(String);

impl StringTable {
    pub fn add(&mut self, string: &str) -> (usize, usize) {
        match self.0.find(string) {
            Some(i) => (i, i + string.len()),
            None => {
                self.0.push_str(string);
                (self.0.len() - string.len(), self.0.len())
            }
        }
    }
}

pub struct AtomTable(Vec<Atom>);

impl AtomTable {
    pub fn new(module: impl Into<Atom>) -> Self {
        Self(vec![module.into()])
    }

    pub fn add(&mut self, atom: impl Into<Atom>) -> u32 {
        self.0.push(atom.into());
        self.0.len() as u32
    }
}

impl IntoIterator for AtomTable {
    type Item = Atom;
    type IntoIter = std::vec::IntoIter<Atom>;

    fn into_iter(self) -> Self::IntoIter {
        self.0.into_iter()
    }
}

#[derive(Debug, Default)]
pub struct Builder {
    atoms: Option<Box<[Atom]>>,
    code: Option<Box<[code::Instr]>>,
    strings: Box<str>,
    imports: Box<[Import]>,
    exports: Box<[Export]>,
    literals: Box<[Term]>,
}

impl Builder {
    pub fn build(self) -> Result<Beam, Error> {
        Ok(Beam {
            atoms: self.atoms.ok_or(Error::MissingChunks)?,
            code: self.code.ok_or(Error::MissingChunks)?,
            strings: self.strings,
            imports: self.imports,
            exports: self.exports,
            literals: self.literals,
        })
    }

    pub fn atoms<I: Into<Box<str>>>(mut self, atoms: impl IntoIterator<Item = I>) -> Self {
        self.atoms = Some(atoms.into_iter().map(|a| a.into()).collect());
        self
    }

    pub fn code(mut self, code: impl IntoIterator<Item = code::Instr>) -> Self {
        self.code = Some(code.into_iter().collect());
        self
    }

    pub fn strings(mut self, strings: impl Into<Box<str>>) -> Self {
        self.strings = strings.into();
        self
    }

    pub fn imports(mut self, imports: impl IntoIterator<Item = Import>) -> Self {
        self.imports = imports.into_iter().collect();
        self
    }

    pub fn exports(mut self, exports: impl IntoIterator<Item = Export>) -> Self {
        self.exports = exports.into_iter().collect();
        self
    }
}
