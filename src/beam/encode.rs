use super::{code::{Arg, Instr}, Beam, BEAM_FORMAT};

pub struct Encoder<'a> {
    beam: &'a Beam,
}

impl<'a> Encoder<'a> {
    pub fn new(beam: &'a Beam) -> Self {
        Self { beam }
    }

    pub fn encode(&self) -> Vec<u8> {
        let mut atu8 = Chunk::new(b"AtU8");

        atu8.write_u32(self.beam.atoms.len() as u32);
        for atom in self.beam.atoms.iter() {
            atu8.data.push(atom.len() as u8);
            atu8.data.extend_from_slice(atom.as_bytes());
        }

        let mut code = Chunk::new(b"Code");
        let mut max_opcode = 0;
        let mut max_label = 0;
        let mut func_count = 0;
        for instr in self.beam.code.iter() {
            max_opcode = max_opcode.max(instr.value());
            if let Instr::Label([Arg(_, m)]) = instr {
                max_label = max_label.max(*m + 1);
            }
            if let Instr::FuncInfo(_) = instr {
                func_count += 1;
            }
        }

        code.write_u32(16);
        code.write_u32(BEAM_FORMAT);
        code.write_u32(max_opcode as u32);
        code.write_u32(max_label);
        code.write_u32(func_count);
        for instr in self.beam.code.iter() {
            code.data.push(instr.value());

            for &Arg(tag, val) in instr.args() {
                if val < 16 {
                    code.data.push(tag as u8 | (val << 4) as u8);
                } else if val < 2048 {
                    code.data
                        .push(tag as u8 | 0b1000 | ((val >> 3) & 0b11100000) as u8);
                    code.data.push(val as u8);
                } else {
                    todo!();
                }
            }
        }

        let mut strt = Chunk::new(b"StrT");
        strt.data.extend_from_slice(self.beam.strings.as_bytes());

        let mut expt = Chunk::new(b"ExpT");
        expt.write_u32(self.beam.exports.len() as u32);
        for export in self.beam.exports.iter() {
            expt.write_u32(export.function);
            expt.write_u32(export.arity);
            expt.write_u32(export.label);
        }

        let mut impt = Chunk::new(b"ImpT");
        impt.write_u32(self.beam.imports.len() as u32);
        for export in self.beam.imports.iter() {
            impt.write_u32(export.module);
            impt.write_u32(export.function);
            impt.write_u32(export.arity);
        }

        let mut beam = Chunk::new(b"FOR1");
        beam.data.extend_from_slice(b"BEAM");
        for chunk in [atu8, code, strt, expt, impt] {
            beam.data.extend_from_slice(&chunk.into_bytes(true));
        }
        beam.into_bytes(false)
    }
}

struct Chunk {
    data: Vec<u8>,
}

impl Chunk {
    fn new(name: &[u8; 4]) -> Self {
        let mut data = name.to_vec();
        data.extend_from_slice(&[0; 4]);
        Self { data }
    }

    fn into_bytes(mut self, pad: bool) -> Vec<u8> {
        let len = self.data.len();
        self.data[4..8].copy_from_slice(&(len as u32 - 8).to_be_bytes());
        if pad {
            self.data.resize(len + (4 - len % 4) % 4, 0);
        }

        self.data
    }

    fn write_u32(&mut self, num: u32) {
        self.data.extend_from_slice(&num.to_be_bytes());
    }
}
