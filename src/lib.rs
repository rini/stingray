// stingray, interfaces for Erlang and the BEAM
// Copyright (c) 2023 rini
//
// SPDX-License-Identifier: Apache-2.0

pub mod beam;
pub mod etf;

pub use beam::Beam;

pub type Result<T, E = Error> = std::result::Result<T, E>;

#[derive(Debug)]
pub enum Error {
    InvalidHeader,
    MissingChunks,
    Incompatible,
    Invalid,
    Eof,
    BadUtf8(std::str::Utf8Error),
    Io(std::io::Error),
}

impl std::error::Error for Error {}

impl std::fmt::Display for Error {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Self::InvalidHeader => write!(f, "invalid BEAM header"),
            Self::MissingChunks => write!(f, "incomplete BEAM file"),
            Self::Incompatible => write!(f, "incompatible BEAM file"),
            Self::Invalid => write!(f, "invalid data"),
            Self::Eof => write!(f, "unexpected eof"),
            Self::BadUtf8(u) => write!(f, "{u}"),
            Self::Io(i) => write!(f, "{i}"),
        }
    }
}

impl From<std::str::Utf8Error> for Error {
    fn from(value: std::str::Utf8Error) -> Self {
        Self::BadUtf8(value)
    }
}

impl From<std::string::FromUtf8Error> for Error {
    fn from(value: std::string::FromUtf8Error) -> Self {
        Self::BadUtf8(value.utf8_error())
    }
}

impl From<std::io::Error> for Error {
    fn from(value: std::io::Error) -> Self {
        Self::Io(value)
    }
}
