// stingray, interfaces for Erlang and the BEAM
// Copyright (c) 2023 rini
//
// SPDX-License-Identifier: Apache-2.0

//! External Term Format, Erlang's Term Format

use crate::Error;

pub const ETF_VERSION: u8 = 131;

#[derive(Debug, Clone)]
pub enum Term {
    Compressed(Box<Term>),
}

impl Term {
    pub fn from_external(data: &[u8]) -> Result<Self, Error> {
        EtfParser::new(data).parse()
    }
}

struct EtfParser<'a> {
    data: &'a [u8],
}

impl<'a> EtfParser<'a> {
    fn new(data: &'a [u8]) -> Self {
        Self { data }
    }

    fn parse(&mut self) -> Result<Term, Error> {
        if self.read_byte()? != ETF_VERSION {
            return Err(Error::Incompatible);
        }

        match self.read_byte() {
            _ => Err(Error::Invalid)
        }
    }

    fn read_byte(&mut self) -> Result<u8, Error> {
        let (&b, data) = self.data.split_first().ok_or(Error::Eof)?;
        self.data = data;
        Ok(b)
    }
}
